/**
 * Tanzil.net Downloader
 * 
 * Sample Link :
 * https://tanzil.net/pub/download/index.php?marks=true&sajdah=true&rub=true&tatweel=true&stanween=true&quranType=uthmani&outType=xml&agree=true
 *  */

const URL = 'https://tanzil.net/pub/download/index.php';
const OPTION = {
  // https://tanzil.net/download/ > #quranType[option][value]
  QURAN_TYPES:[
    "uthmani",
    "uthmani-min",
    "simple",
    "simple-clean",
    "simple-plain",
    "simple-min",
  ],
  // https://tanzil.net/download/ > #outType[option][value]
  OUTPUT_TYPES:[
    "txt",
    "txt-2",
    "xml",
    "sql",
  ],
  NAMES: [
    "marks",
    "sajdah",
    "rub",
    "tatweel",
    "stanween",
    "quranType",
    "outType",
    "agree",
  ],
  DEFAULT : {
    "marks": true,
    "sajdah": true,
    "rub": true,
    "tatweel": true,
    "stanween": true,
    "quranType": "uthmani",
    "outType": "xml",
    "agree": true,
  }
};

const option_to_str = opt=>Object.entries(opt).map(pair=>pair.join('=')).join('&')
const url_option_str = (url=URL,option=OPTION.DEFAULT) => `${url}?${option_to_str(option)}`

console.log('url',url_option_str())
// 