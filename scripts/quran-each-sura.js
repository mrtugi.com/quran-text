/**
 * main.js
 * Folder Generator
 * Generate folder with surah json file as contents
 */
const fs = require("fs-jetpack")
const files = [
  'quran-simple-clean.xml.json',
  'quran-simple-min.xml.json',
  'quran-simple-plain.xml.json',
  'quran-simple.xml.json',
  'quran-uthmani-min.xml.json',
  'quran-uthmani.xml.json',
]
files.forEach(file=>{
  json = fs.read(file,'json')
  console.log('folder : ',json.type)
  fs.dir(json.type)

  json.quran.sura.forEach(sura=>{
    filename = sura.index+'.json'
    sura.copyright = json.copyright
    sura.type = json.type
    sura.version = json.version
    // console.log('sura',sura)
    console.log('name : ',filename)
    // fs.remove(json.type+'/'+filename)
    fs.write(json.type+'/'+filename,sura)
  })
})
