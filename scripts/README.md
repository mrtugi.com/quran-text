# Al-Quran text tool & utility scripts

Scripts files for Al-Quran text tool & utility, mainly using javascripts and bash.

## Setup

Using Yarn

```
yarn
```

Using NPM

```
npm install
```

## Dependencies

NodeJS (`package.json`)

```
"dependencies": {
  "fs-jetpack": "^4.3.1" # File system manipulation library
}
```

## Scripts

### Tanzil.net Downloader

- file : `quran-download-tanzil.js`
- documentations : [Usage]() - [Examples]()
