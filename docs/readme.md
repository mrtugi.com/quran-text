# Quran Text

Al-Qur'an text from tanzil.net
https://tanzil.net/download/

## Documentations

- [Scripts](docs/scripts-index.md)

## Folder Structure

```
.
├── .gitignore                      # Ignored files by git
├── .gitlab-ci.yml                   # Continous Integration Configuration using GitLab
├── docs                            # Documentation files
│   ├── scripts-index.md            # Scripts folder index documentation
│   └── readme.md                   # Git Index Page
├── README.md -> docs/readme.md     # Git Index Page linked to docs/readme.md
├── quran                           # Main output folder
│   ├── uthmani
│   ├── uthmani-min
│   └── ...
├── scripts                         # Tools and utilities scripts
│   ├── package.json                # NPM & yarn Package file
│   ├── README.md                   # Index Scripts Documentation Page linked to docs/scripts-index.md
│   ├── quran-download-tanzil.js    # Downloader for src/ folder
│   ├── quran-each-sura.js          # Surah file generator (initially generate quran/*/*.json file)
│   └── ...
└── src                             # Main source files, downloaded and re-formatted files
    ├── quran-simple-clean.xml      # Downloaded file
    ├── quran-simple-clean.xml.json # Re-formatted file from xml to json
    └── ...
```

# License

https://tanzil.net/docs/Text_License
Permission is granted to copy and distribute verbatim copies of the Quran text provided here, but changing the text is not allowed. The text can be used in any website or application, provided that its source (Tanzil Project) is clearly indicated, and a link is made to tanzil.net to enable users to keep track of changes.
